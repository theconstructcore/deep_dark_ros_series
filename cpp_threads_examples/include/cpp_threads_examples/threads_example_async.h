
#ifndef THREAD_EXAMPLE_H
#define THREAD_EXAMPLE_H

#include <ros/ros.h>

#include <nav_msgs/Odometry.h>
#include <sensor_msgs/Imu.h>

class ThreadExampleClass {
public:
  ThreadExampleClass();
  virtual ~ThreadExampleClass();

private:
  // ROS NodeHandle
  ros::NodeHandle node_handle_;

  // reate Assync spiner
  ros::AsyncSpinner spinner;

  // PUBLISHERS
  ros::Subscriber imu_sub;
  ros::Subscriber odom_sub;

  void IMU_callback(const sensor_msgs::Imu::ConstPtr &msg);
  void ODOM_callback(const nav_msgs::Odometry::ConstPtr &msg);
};

#endif // THREAD_EXAMPLE_H