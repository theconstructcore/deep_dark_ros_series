#include "cpp_threads_examples/threads_example.h"

ThreadExampleClass::ThreadExampleClass() : node_handle_("") {

  ROS_INFO("Init Started");

  imu_sub = node_handle_.subscribe("/box_bot/imu/data3", 10,
                                   &ThreadExampleClass::IMU_callback, this);
  odom_sub = node_handle_.subscribe("/box_bot/odom", 10,
                                    &ThreadExampleClass::ODOM_callback, this);

  ROS_INFO("Init Finished");
}

ThreadExampleClass::~ThreadExampleClass() { ros::shutdown(); }

void ThreadExampleClass::IMU_callback(const sensor_msgs::Imu::ConstPtr &msg) {
  float x_vel = msg->angular_velocity.x;
  float y_vel = msg->angular_velocity.y;
  ros::Duration(5.0).sleep();
  ROS_INFO("IMU VEL [%f,%f]", x_vel, y_vel);
}

void ThreadExampleClass::ODOM_callback(
    const nav_msgs::Odometry::ConstPtr &msg) {
  float x_pos = msg->pose.pose.position.x;
  float y_pos = msg->pose.pose.position.y;

  ROS_INFO("ODOM POS [%f,%f]", x_pos, y_pos);
}

int main(int argc, char **argv) {
  // Init ROS node
  ros::init(argc, argv, "thread_example_node");
  ThreadExampleClass thread_ex_object;

  ros::spin();

  return 0;
}
